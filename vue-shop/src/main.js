import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import router from './router/router.js'
import './assets/global.css'
import * as ElIconModules from '@element-plus/icons-vue'
import axios from 'axios'
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'
import installFilter from './filter/filters'

const app = createApp(App)

//  统一注册el-icon图标
for(let iconName in ElIconModules){
    app.component(iconName,ElIconModules[iconName])
}

const axiosInstance = axios.create({
    baseURL: 'http://127.0.0.1:8888/api/private/v1/', // 设置你的API基础URL
    timeout: 5000, // 设置请求超时时间（可选）
  });
axiosInstance.interceptors.request.use(config => {
  //console.log(config);
  config.headers.Authorization = window.sessionStorage.getItem('token')
  //最后必须 return config
  return config
})
// 异常
// axiosInstance.defaults.headers["Authorization"] = window.sessionStorage.getItem('token');
  // 将axios实例绑定到Vue应用上
app.config.globalProperties.$http = axiosInstance;

app.use(ElementPlus, {
  // 其他组件库配置...
  $message: {
    duration: 3000, // 消息显示3秒后自动关闭，你可以根据需要调整这个值
    showClose: true, // 是否显示关闭按钮，默认为 false
    // 其他相关配置...
  }
});

installFilter(app)

function useTable(app){
  app.use(VXETable)
}

app.use(useTable)
app.use(router,ElementPlus)
app.mount('#app')
