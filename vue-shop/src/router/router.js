import { createRouter,createWebHistory } from 'vue-router'
import login from '../components/Login.vue'
import home from '../components/Home.vue'
import welcome from '../components/welcome.vue'
import user from '../components/user/User.vue'
import rights from '../components/power/Rights.vue'
import roles from '../components/power/Roles.vue'
import cate from '../components/goods/Cate.vue'
import params from '../components/goods/Params.vue'
import test from '../components/goods/test.vue'
import list from '../components/goods/List.vue'
import add from '../components/goods/Add.vue'
import orders from '../components/order/Order.vue'
import reports from '../components/report/Report.vue'

const routes = [
    {
        path:'/login',
        component : login 
    },
    {
        path: '/home',
        component : home,
        // 重定向welcome
        redirect : '/welcome',
        children : [
            {path : '/welcome',component : welcome},
            {path : '/users',component : user},
            {path : '/rights',component : rights},
            {path : '/roles',component : roles},
            {path : '/categories',component : cate},
            {path : '/params',component : params},
            {path : '/test',component : test},
            {path : '/goods',component : list},
            {path : '/goods/add',component : add},
            {path : '/orders',component : orders},
            {path : '/reports',component : reports},
        ]
    }
]
const router = new createRouter({
    history: createWebHistory(),
    routes
})

// 挂载路由守卫对象
router.beforeEach((to,from,next) => {
    // to 将要被访问的路径
    // from 表示从哪个路径跳转而来
    // next 一个函数，表示放行
    // next() 放行 next('/login') 强制跳转

    if(to.path == '/login') return next()
    // debugger;
    // 获取token
    const tokenStr = window.sessionStorage.getItem('token')
    if(!tokenStr){
        return next('/login')
    }else{
        next()
    }
})

export default router